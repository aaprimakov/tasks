const router = require('express').Router();
const { getIo } = require('@ijl/cli/src/server');
const cors = require('cors');

const baseTime = process.env === 'production' ? 0 : 1000;

router.use(cors());

const wait = (time = baseTime) => (req, res, next) => setTimeout(next, time);
wait.slow = wait(baseTime * 1.4);
wait.fast = wait(baseTime * 0.3);

const firstTask = {
    "id": "1234jk23bj424kjh5k3b454h5k34",
    "title": "Task 1 try to win",
    "number": 1,
    tags: ["stc-21-03"],
    "rating": 5.0201,
    description: `## Сложение двух параметров

Напишите функцию сложения __двуз__ **параметров**.
    
> что-то вот тут
> Сказано вобщем по существу

![апельсин](https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg)

    `,
    "difficulty": "Easy",
    template: `/**
* Функция сложения двух параметров
* 
* @prop a {number} - Првый опперанд.
* @prop b {number} - Второй опперанд.
* @return
*/
function funcName(a, b) {

}`,
    tests: `expect(true).toBe(true);

expect(false).toBe(false);

expect(funcName()).toBe(undefined);

expect(funcName()).toBe('error');
`,
    answers: new Map([
    ['asdfsvd89d7sa9cdsc9ay9s', { text: `some code here ...` }]
])}

const getError = text => ({
    success: false,
    error: {
        message: text,
    }
});

const stubs = {
    tasks: 'success'
}

const craeteUserStorage = () => {
    const _users = new Map();

    return {
        addUser(userId) {
            _users.set(userId, { id: userId });
        },
        getUser(userId) {
            return _users.get(userId);
        },
        updateUser(userId, data) {
            const user = _users.get(userId) || {};
            _users.set(userId, { ...user, ...data })
        },
        deleteUser(userId) {
            _users.delete(userId);
        }
    }
}

const createTasksStorage = (initial = []) => {
    const taskIdBase = '1234jk23bj424kjh5k3b454h5k34';
    let taskIdInt = 1;
    const _tasks = new Map([
        ...initial
    ]);

    const getTask = (taskId) => {
        const task = _tasks.get(taskId);

        if (!task) throw new Error(`No task with taskId = ${taskId} found`);

        return task
    }

    return {
        newTaskId() {
            const newTaskId = taskIdBase + taskIdInt++;
            _tasks.set(newTaskId, {
                id: newTaskId,
                number: taskIdInt,
                title: 'new Task title',
                description: '## type task here\n',
                answers: new Map(),
                template: 'function funcName(...args) {\n\n}',
                tests: 'expect(true).toBe(true);\nexpect(false).toBe(false);'
            })

            return newTaskId
        },
        getTask(taskId) {
            return _tasks.get(taskId);
        },
        updateTask(taskData) {
            const { answers } = _tasks.get(taskData.id) || { answers: new Map()};
            
            _tasks.set(taskData.id, { ...taskData, answers });
        },
        newAnswer(taskId) {
            const task = getTask(taskId);
            const count = task?.answers.keys()?.length || 0;
            const base = 'asdfsvd89d7sa9cdsc9ay9s';
            const nextKey = base + count + 1;
            task.answers.set(nextKey, { text: 'type answer here...' })

            return nextKey
        },
        getTasksList() {
            return [ ..._tasks.values()].map(({answers, ...t}) => ({ ...t }));
        },
        updateAnswer({ taskId, answerId, value }) {
            const task = getTask(taskId);
            const answer = task.answers.get(answerId);
            const updatedAnswer = { ...answer, text: value };

            task.answers.set(answerId, updatedAnswer);
            return updatedAnswer;
        },
        getAnswer({ taskId, answerId }) {
            const task = getTask(taskId);

            return task.answers.get(answerId);
        }
    }
}

const userStorage = craeteUserStorage();
const tasksStorage = createTasksStorage([[
    '1234jk23bj424kjh5k3b454h5k34', firstTask
]]);

const io = getIo();
io.on('connection', (socket) => {
    userStorage.addUser(socket.id);
    
    socket.on('subscribeOnAnswer', ({ taskId, answerId }) => {
        const user = userStorage.getUser(socket.id);
        const roomId = `${taskId}:${answerId}`;

        socket.join(roomId);

        const currentTaskAnswer = tasksStorage.getAnswer({ taskId, answerId });
        userStorage.updateUser(user.id, { roomId, taskId, answerId });
        io
            .to(roomId)
            .emit('answerUpdated', currentTaskAnswer);
    })

    socket.on('updateTask', (data) => {
        tasksStorage.updateTask(data)
    })
    
    socket.on('updateAnswer', ({ text }) => {
        const user = userStorage.getUser(socket.id);
        const updatedAnswer = tasksStorage.updateAnswer({ taskId: user.taskId, answerId: user.answerId, value: text });
        io
            .to(user.roomId)
            .emit('answerUpdated', updatedAnswer)
    })

    socket.on('disconnect', () => {
        userStorage.deleteUser(socket.id);
    });
});

router.get('/tasks', wait.fast, (req, res) => {
    if (stubs.tasks === 'error') {
        return res.status(400).send(getError('Список задач потерялся по дороге к вам. Уже ищем)'));
    }

    res.send({
        data: tasksStorage.getTasksList()
    });
});

router.post('/answer/new', (req, res) => {
    const { taskId } = req.body;
    const newAnswerId = tasksStorage.newAnswer(taskId);
    
    res.send({
        answerId: newAnswerId
    })
});

router.post('/task/new', wait.slow, (req, res) => {
    const newTaskId = tasksStorage.newTaskId();

    res.send({
        taskId: newTaskId,
    })
});

router.get('/task/:taskId', (req, res) => {
    const { taskId } = req.params;

    const task = tasksStorage.getTask(taskId);
    res.send({ task });
});

router.get('/admin/set/:key/:value', wait.slow, (req, res) => {
    const {
        key,
        value
    } = req.params;
    stubs[key] = value;
    res.send('')
});

router.get('/admin', (req, res) => {
    res.send(`
        <h1>Админка стабов</h1>
        <ul>
            <li>
                <h2>/tasks</h2>

                <ul>
                    <li><button onclick="fetch('/api/admin/set/tasks/success')">success</button></li>
                    <li><button onclick="fetch('/api/admin/set/tasks/error')">error</button></li>
                </ul>
            </li>
        </ul>
    `)
});

module.exports = router;
