import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import { URLs } from './__data__/urls';
import { Tasks } from './pages/tasks';
import { TaskPage } from './pages/task';
import { EditTaskPage } from './pages/edit-task';

export const Dashboard = () => {
    return (
        <Routes>
            <Route
                path={URLs.baseUrl}
                element={<Navigate replace to={URLs.tasks.url} />}
            />
            <Route path={URLs.newTask.url} element={<EditTaskPage />} />
            <Route path={URLs.editTask.url} element={<EditTaskPage />} />
            <Route path={URLs.tasks.url} element={<Tasks />} />
            <Route path={URLs.task.url} element={<TaskPage />} />
            <Route path={URLs.taskAnswer.url} element={<TaskPage />} />
        </Routes>
    );
};
