import React from 'react';
import i18next from 'i18next';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import { Link as ConnectedLink } from 'react-router-dom';

import { URLs } from '../../__data__/urls';

const ConnectedTasksLink = ({ children, onClick = () => {} }) => (
    <Link
        color="white"
        onClick={onClick}
        component={ConnectedLink}
        to={URLs.tasks.url}
        underline="hover"
    >
        <Box sx={{ display: 'flex' }}>
            <ArrowBackIcon />
            {children}
        </Box>
    </Link>
);

export const Header = ({ children }) => {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Box sx={{ flexGrow: 1, display: { md: 'flex' }, color: 'white' }}>
                        <Typography variant='h6' component="div" sx={{ flexGrow: 1 }}>{i18next.t('tasks.header.main').toUpperCase()}</Typography>
                        {children}
                        <ConnectedTasksLink>
                            {i18next.t('tasks.header.back.to.list')}
                        </ConnectedTasksLink>
                    </Box>
                </Toolbar>
            </AppBar>
        </Box>
    )
}
