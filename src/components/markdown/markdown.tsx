import Typography from '@mui/material/Typography';
import React from 'react';
import ReactMarkdown from 'react-markdown'; 

export const Markdown = ({ markdown }) => {
    return (
        <ReactMarkdown
            children={markdown}
            components={{
                h1: ({ node, ...props }) => <Typography variant='h2' {...props} />,
                h2: ({ node, ...props }) => <Typography variant='h3' {...props} />,
                h3: ({ node, ...props }) => <Typography variant='h4' {...props} />,
                h4: ({ node, ...props }) => <Typography variant='h5' {...props} />,
                h5: ({ node, ...props }) => <Typography variant='h6' {...props} />,
                h6: ({ node, ...props }) => <Typography variant='subtitle1' {...props} />
            }}
        />
    )
}
