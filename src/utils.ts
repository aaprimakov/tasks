import { LOCALE_ANSWERS_KEY } from './__data__/constants';

export const getAnswerKey = taskId => `${LOCALE_ANSWERS_KEY}-${taskId}`;
