import { getNavigations, getNavigationsValue } from '@ijl/cli';
import pkg from '../../package.json';

const baseUrl = getNavigationsValue(`${pkg.name}.main`);
const navs = getNavigations();
const makeUrl = url => baseUrl + url;

export const URLs = {
    baseUrl,
    tasks: {
        url: makeUrl(navs[`link.${pkg.name}.list`]),
    },
    task: {
        url: makeUrl(navs[`link.${pkg.name}.single`]),
        getTaskUrl(taskId) {
            return makeUrl(
                navs[`link.${pkg.name}.single`]?.replace(':taskId', taskId)
            );
        },
    },
    newTask: {
        url: makeUrl(navs[`link.${pkg.name}.new`])
    },
    editTask: {
        url: makeUrl(navs[`link.${pkg.name}.edit`]),
        getUrl: taskId => makeUrl(navs[`link.${pkg.name}.edit`]?.replace(':taskId', taskId))
    },
    taskAnswer: {
        url: makeUrl(navs[`link.${pkg.name}.single.answer`]),
        getUrl(taskId, answerId) {
            return makeUrl(
                navs[`link.${pkg.name}.single.answer`]?.replace(':taskId', taskId)?.replace(':answerId', answerId)
            );
        },
    },
};
