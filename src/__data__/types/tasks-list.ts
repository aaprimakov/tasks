export enum Difficulty {
    Easy = 'Easy',
    Medium = 'Medium',
    Hard = 'Hard',
}

export type TTaskListItem = {
    id: string;
    title: string;
    rating?: number;
    number: number;
    difficulty: Difficulty;
};
