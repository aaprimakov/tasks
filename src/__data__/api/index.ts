import { getConfigValue } from '@ijl/cli';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Task } from '../model';

const apiPath = getConfigValue('tasks.api.key');

export const api = createApi({
    baseQuery: fetchBaseQuery({
        baseUrl: apiPath,
    }),
    keepUnusedDataFor: 0,
    endpoints: builder => ({
        tasks: builder.query({
            query: () => ({
                url: getConfigValue('tasks.api.list'),
            }),
        }),
        createAnswer: builder.query({
            query: (taskId: string) => ({
                url: getConfigValue('tasks.api.create.answer'),
                method: 'POST',
                body: {
                    taskId
                }
            })
        }),
        createTask: builder.query({
            query: () => ({
                url: getConfigValue('tasks.api.create.task'),
                method: 'POST'
            })
        }),
        getTaskData: builder.query<{ task: Task }, string>({
            query: taskId => ({
                url: getConfigValue('tasks.api.get.task')?.replace(':taskId', taskId)
            })
        })
    }),
});

export const { useTasksQuery, useLazyCreateAnswerQuery, useLazyCreateTaskQuery, useLazyGetTaskDataQuery, reducerPath, reducer, middleware } = api;
