export type Task = {
    id: string;
    title: string;
    description: string;
    tags: string[];
    tests: string;
    template: string;
}

export type Answer = {
    text: string;
}
