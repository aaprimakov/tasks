import { createSelector } from '@reduxjs/toolkit';

import { TTaskListItem } from '../types/tasks-list';

export const tasksListSelector = createSelector(
    (state: { data: TTaskListItem[] }) => state,
    (info: { data: TTaskListItem[] }) =>
        info?.data.map(t => ({
            ...t,
            rating: getRating(t.rating),
        }))
);

const getRating = rawRating => {
    const ratingNum = Number(rawRating);
    let rating;
    if (!Number.isNaN(rating)) {
        rating = Math.floor(100 * ratingNum) / 100;
    }
    return rating;
};
