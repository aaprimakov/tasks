import { configureStore } from '@reduxjs/toolkit';

import { middleware, reducer, reducerPath } from './api';
import { reducer as editTaskReducer } from './slices/edit-task';

export const store = configureStore({
    reducer: {
        editTask: editTaskReducer,
        [reducerPath]: reducer
    },
    middleware: dgm => dgm().concat(middleware),
})

export type Store = ReturnType<typeof store.getState>