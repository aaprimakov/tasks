import { createSelector } from '@reduxjs/toolkit';

import { Store } from '../store';

export const rootSelector = createSelector((state: Store) => state, state => state);
