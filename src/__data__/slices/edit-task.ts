import { createSlice, createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './index';

const initialValue = {
    descriptionValue: ''
}

const { reducer, actions } = createSlice({
    name: 'edit-task',
    initialState: {
        ...initialValue
    },
    reducers: {
        reset(state) {
            Object.assign(state, initialValue);
        },
        changeDescr(state, { payload }) {
            state.descriptionValue = payload;
        }
    }
})

const sliceSelector = createSelector(rootSelector, state => state.editTask)
const description = createSelector(sliceSelector, state => state.descriptionValue)

export const selectors = {
    description
}

export {
    actions,
    reducer
}