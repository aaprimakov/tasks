import { useState } from 'react';

export const useTests = (testsString, answerString) => {
    const [results, setResults] = useState([]);

    const addResult = (res) => {
        setResults(c => [...c, res])
    }

    const expect = (funcReturn) => {
        return {
            toBe: (checkValue) => {
                addResult({
                    pass: checkValue === funcReturn,
                    expect: checkValue,
                    output: funcReturn
                })
            }
        }
    }

    return {
        results,
        run: () => {
            setResults([]);
            eval(`${answerString};${testsString}`);
            console.log(results)
        }
    }
}
