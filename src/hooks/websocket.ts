import { useEffect, useRef, useState } from 'react';
import { getConfigValue } from '@ijl/cli';
import { Manager, Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import { Task } from '../__data__/model';

const manager = new Manager(getConfigValue('tasks.ws'), {
  reconnectionDelayMax: 10000,
  path: getConfigValue('tasks.ws.path')
});

export const useWebSocket = <T>() => {
    const socketRef = useRef<Socket<DefaultEventsMap, DefaultEventsMap>>();
    const [value, setValue] = useState<T>();

    useEffect(() => {
        if (!socketRef.current) {
            socketRef.current = manager.socket('/');
        }

        // return () => manager.removeListener()
    }, []);

    return {
        value,
        saveAnswer: (value) => {
            socketRef.current.emit('updateAnswer', { text: value });
        },
        subOnAnswer: (taskId, answerId) => {
            socketRef.current.emit('subscribeOnAnswer', { taskId, answerId });
            socketRef.current.on('answerUpdated', (nextValue) => {
                console.log('answerUpdated', nextValue)
                setValue(nextValue);
            });
        },
        saveTask: (data: Task) => {
            socketRef.current.emit('updateTask', data)
        }
    }
}