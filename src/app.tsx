import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import CssBaseline from '@mui/material/CssBaseline';

import { store } from './__data__/store';
import { Dashboard } from './dashboard';

const App = () => {
    return (
        <BrowserRouter>
            <Provider store={store}>
                <CssBaseline />
                <Dashboard />
            </Provider>
        </BrowserRouter>
    );
};

export default App;
