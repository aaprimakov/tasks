import React from 'react';
import Container from '@mui/material/Container';
import i18next from 'i18next';
import AddIcon from '@mui/icons-material/Add';
import Table from '@mui/material/Table';
import Th from '@mui/material/TableHead';
import Tb from '@mui/material/TableBody';
import Tr from '@mui/material/TableRow';
import Tc from '@mui/material/TableCell';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import { Link as ConnectedLink } from 'react-router-dom';
import { useWebSocket } from '../../hooks/websocket';

import { useTasksQuery } from '../../__data__/api';
import { URLs } from '../../__data__/urls';
import { tasksListSelector } from '../../__data__/selectors/tasks';

import { ColoredCircule, STR } from './styles';
import { getAnswerKey } from '../../utils';

const ConnectedTaskLink = ({ id, children, onClick = () => {} }) => (
    <Link
        color="CaptionText"
        onClick={onClick}
        component={ConnectedLink}
        to={URLs.task.getTaskUrl(id)}
        underline="hover"
    >
        {children}
    </Link>
);

export const Tasks = () => {
    const { data, error, isLoading, refetch } = useTasksQuery(null);
    const tasks = tasksListSelector(data);
    useWebSocket();

    const getHandleNewAnswerClick = taskId => () => {
        localStorage.removeItem(getAnswerKey(taskId));
    }

    return (
        <Container fixed>
            <Box sx={{ paddingBottom: 2, marginTop: 3 }}>
                <Typography variant="h2">{i18next.t('tasks.list.page.header')}</Typography>
            </Box>

            {error ? (
                <Alert
                    severity="error"
                    action={
                        <Button color="inherit" size="small" onClick={refetch}>
                            {i18next.t('tasks.error.repeat')}
                        </Button>
                    }
                >
                    <AlertTitle>{i18next.t('tasks.error.common.title')}</AlertTitle>
                    {(error as any).data?.error?.message ||
                        i18next.t('tasks.error.common.text')}
                </Alert>
            ) : <>
                <Link
                    color="CaptionText"
                    component={ConnectedLink}
                    to={URLs.newTask.url}
                    underline="hover"
                >
                    {i18next.t('tasks.list.page.new.task')}
                </Link>
                <Paper>
                    <Table>
                        <Th>
                            <Tr>
                                <Tc>{i18next.t('tasks.list.page.table.header.number')}</Tc>
                                <Tc>{i18next.t('tasks.list.page.table.header.title')}</Tc>
                                <Tc>{i18next.t('tasks.list.page.table.header.rating')}</Tc>
                                <Tc></Tc>
                            </Tr>
                        </Th>
                        <Tb>
                            {tasks?.map(task => (
                                <STR key={task.id}>
                                    <Tc>
                                        <ConnectedTaskLink id={task.id}>
                                            {task.number}
                                        </ConnectedTaskLink>
                                    </Tc>
                                    <Tc>
                                        <ConnectedTaskLink id={task.id}>
                                            <Typography>
                                                {task.title}
                                            </Typography>
                                        </ConnectedTaskLink>
                                    </Tc>
                                    <Tc>
                                        <ConnectedTaskLink id={task.id}>
                                            <Typography>
                                                {task.rating || i18next.t('tasks.list.page.table.not.rated')}
                                            </Typography>
                                        </ConnectedTaskLink>
                                    </Tc>
                                    <Tc>
                                        <ConnectedTaskLink onClick={getHandleNewAnswerClick(task.id)} id={task.id}>
                                            <Box sx={{ display: 'flex' }}>
                                                <AddIcon />
                                                <Typography>
                                                    {i18next.t('tasks.list.page.new.answer')}
                                                </Typography>
                                            </Box>
                                        </ConnectedTaskLink>
                                    </Tc>
                                </STR>
                            ))}
                        </Tb>
                    </Table>
                </Paper>
            </>}

            {isLoading && (
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        marginTop: 2,
                    }}
                >
                    <ColoredCircule error={error} />
                </Box>
            )}
        </Container>
    );
};
