import React from 'react';
import { styled } from '@mui/material/styles';
import TableRow from '@mui/material/TableRow';
import CircularProgress from '@mui/material/CircularProgress';

export const STR = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

export const ColoredCircule = styled<
    React.FC<{ className?: string; error: any }>
>(({ className }) => (
    <CircularProgress className={className} color="inherit" />
))`
    color: ${({ error, theme }) =>
        error ? theme.palette.error.main : theme.palette.success.main};
`;
