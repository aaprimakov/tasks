import React from 'react';
import { Navigate, useParams } from 'react-router-dom';
import Box from '@mui/material/Box';
import Loader from '@mui/material/CircularProgress'

import { URLs } from '../../../__data__/urls';

import { useAnswerId } from '../hooks/answer-id-hook';

export const withAnswerId = (Component) => () => {
    const { taskId, answerId } = useParams();
    const { answerId: myAnswerId, loading } = useAnswerId(taskId, answerId);

    if (!answerId && myAnswerId) {
        return (
            <Navigate replace to={URLs.taskAnswer.getUrl(taskId, myAnswerId)} />
        )
    }

    if (loading) {
        return <Box sx={{ mx: "auto", width: 200, height: '100vh' }}><Loader /></Box>
    }

    return <Component />
}
