import React, { useEffect, useState, useLayoutEffect } from 'react';
import { useParams, Link as ConnectedLink } from 'react-router-dom';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Loader from '@mui/material/CircularProgress';
import { Typography } from '@mui/material';

import { useWebSocket } from '../../hooks/websocket';
import { URLs } from '../../__data__/urls';
import { Header, Markdown } from '../../components';
import { useLazyGetTaskDataQuery } from '../../__data__/api';
import { useMonako } from '../../hooks/useMonaco';
import { Answer } from '../../__data__/model';

import { withAnswerId } from './hocs/with-answer-id';
import {  } from './style';

const ConnectedEditTaskLink = ({ id, children, onClick = () => {} }) => (
    <Link
        color="CaptionText"
        onClick={onClick}
        component={ConnectedLink}
        to={URLs.editTask.getUrl(id)}
        underline="hover"
    >
        {children}
    </Link>
);

const TaskPage = () => {
    const { taskId, answerId } = useParams();
    const { saveAnswer, subOnAnswer, value: answer } = useWebSocket<Answer>();
    const [getTaskData, { data: taskData, isSuccess }] = useLazyGetTaskDataQuery();
    const [tr, triggerMonako] = useState(false);
    const { nodeRef, getValue } = useMonako(answer?.text, tr, 'javascript', (value) => {
        saveAnswer(value);
    });

    useEffect(() => {
        if (taskId && !taskData) {
            getTaskData(taskId);
        }
    }, [taskId, taskData]);
    useEffect(() => {
        console.log('useEffect :45')
        if (answer?.text !== getValue()) {
            triggerMonako(t => !t);
        }
    }, [answer?.text, triggerMonako, getValue])
    
    useEffect(() => {
        if (taskId && answerId) {
            console.log('sub on answer')
            subOnAnswer(taskId, answerId)
        }
    }, [taskId, answerId])

    // useLayoutEffect(() => {
    //     const pointer = areaRef.current.selectionStart;
    //     areaRef.current.value = value;

    //     window.requestAnimationFrame(() => {
    //         areaRef.current.selectionStart = pointer;
    //         areaRef.current.selectionEnd = pointer;
    //     });
    // }, [value])

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <Header>
                    <ConnectedEditTaskLink id={taskId}>Редактировать (не переведено)</ConnectedEditTaskLink>
                </Header>
            </Box>
            {!isSuccess && <Loader />}
            {isSuccess && (
                <Box sx={{
                    width: '100%',
                    display: 'flex',
                    height: 800,
                    minHeight: 800,
                }}>
                    <Box sx={{ 
                        pt: 2,
                        display: 'flex',
                        flexDirection: 'column',
                        overflowY: 'scroll',
                        width: '50%'
                    }}>
                        <Typography variant="h3">{taskData.task.title}</Typography>
                        <Markdown markdown={taskData.task.description} />
                    </Box>
                    <Box sx={{ 
                        pt: 2,
                        display: 'flex',
                        flexDirection: 'column',
                        overflowY: 'scroll',
                        width: '50%'
                    }}>
                        <Box sx={{ width: '100%', height: '100%', background: '#ccc', padding: '4px' }}><div style={{ height: '100%' }} ref={nodeRef} /></Box>
                    </Box>
                    
                </Box>
            )}
        </>
    )
}

export default withAnswerId(TaskPage);

