import { useRef, useState } from 'react';

import { useLazyCreateAnswerQuery } from '../../../__data__/api';
import { getAnswerKey } from '../../../utils';

export const useAnswerId = (taskId, answId) => {
    const [answerId, setAnswerId] = useState<string>(null);
    const [trigger, answer] = useLazyCreateAnswerQuery();
    const onceRef = useRef(false);

    if (!onceRef.current && answId) {
        onceRef.current = true;
        if (!localStorage.getItem(getAnswerKey(taskId))) {
            localStorage.setItem(getAnswerKey(taskId), answId);
        }
    }

    if (!answerId && answer.isSuccess) {
        setAnswerId(answer.data.answerId)
    }

    if (!answerId && !answId && answer.isUninitialized) {
        const id = localStorage.getItem(getAnswerKey(taskId));

        if (id) {
            setAnswerId(id);
        } else {
            trigger(taskId)
        }
    }

    return { answerId: answerId || answId, loading: answer.isLoading };
}