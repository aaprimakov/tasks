// import { styled } from '@mui/material/styles';
import styled from '@emotion/styled'

export const Wrapper = styled.div`
    width: 100%;
`;

export const Main = styled.main<{ size: 'large' | 'small'}>`
    display: flex;
    height: 812px;
    max-height: ${({ size }) => size === 'large' ? 812 : 612}px;
    width: 100%;
    padding: 12px;
`;

export const Haf = styled.div`
    width: 50%;
    padding-top: 24px;
    display: flex;
    flex-direction: column;
`;