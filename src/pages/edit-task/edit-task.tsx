import i18next from 'i18next';
import React, { useState, useRef, useEffect } from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import { Task } from '../../__data__/model';
import { Header } from '../../components';

import { Wrapper, Main } from './styles';
import DescriptionTab from './description-tab';
import TestsTab from './tests-tab';

enum TABS {
    DESCRIPTION,
    TESTS
}

const EditTaskPage = () => {
    const [tab, setTab] = useState(TABS.DESCRIPTION);
    const handleChangetab = (_event, nextTab: TABS) => setTab(nextTab);

    return (
        <Wrapper>
            <Header/>
            <Tabs value={tab} onChange={handleChangetab} aria-label="basic tabs example">
                <Tab
                    label="Описание задачи (не переведено)"
                    id={`${TABS.DESCRIPTION}`}
                    aria-controls={`tabpanel-${TABS.DESCRIPTION}`}
                />
                <Tab
                    label="Тесты (не переведено)"
                    id={`${TABS.TESTS}`}
                    aria-controls={`tabpanel-${TABS.TESTS}`}
                />
            </Tabs>
            <TabPanel index={TABS.DESCRIPTION} value={tab}>
                <DescriptionTab />
            </TabPanel>
            <TabPanel index={TABS.TESTS} value={tab}>
                <TestsTab />
            </TabPanel>
        </Wrapper>
    )
}

const TabPanel = ({ children, value, index, ...other }) => (
    <div
        role="tabpanel"
        hidden={value !== index}
        id={`tabpanel-${index}`}
        aria-labelledby={`tab-${index}`}
        {...other}
    >
        {value === index && (
            children
        )}
    </div>
);

export default EditTaskPage