import React, { useState } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import DoneIcon from '@mui/icons-material/Done';
import CloseIcon from '@mui/icons-material/Close';
import { useParams } from 'react-router-dom';

import { useWebSocket } from '../../hooks/websocket';
import { Task } from '../../__data__/model';

import { withTask } from './hocs/with-task-id';
import { useMonako } from '../../hooks/useMonaco';
import { useTests } from '../../hooks/useTests';
import { Haf, Main } from './styles';

const TestsTab: React.FC<{ task: Task }> = ({ task: currentTaskParams }) => {
    const { saveTask } = useWebSocket();
    const { taskId } = useParams();
    const [example, setExample] = useState(currentTaskParams.template);
    const { results, run } = useTests(currentTaskParams.tests, example);

    const { nodeRef } = useMonako(currentTaskParams.tests, null, 'javascript', (tests) => {
        saveTask({
            id: taskId,
            ...currentTaskParams,
            tests
        });
    });
    const { nodeRef: nodeTemplateRef } = useMonako(currentTaskParams.template, null, 'javascript', (template) => {
        saveTask({
            id: taskId,
            ...currentTaskParams,
            template
        });
    });
    const { nodeRef: nodeExampleRef } = useMonako(example, null, 'javascript', setExample);

    return (
        <>
            <Main size="small">
                <Haf>
                    <Typography variant='h5'>напишите тесты</Typography>

                    <Box sx={{ width: '100%', height: '100%', background: '#ccc', padding: '4px' }}><div style={{ height: '100%' }} ref={nodeRef} /></Box>
                </Haf>
                <Haf>
                    <Typography variant='h5'>Шаблон для пользователей</Typography>
                    <Box sx={{ width: '100%', height: '100%', background: '#ccc', padding: '4px' }}><div style={{ height: '100%' }} ref={nodeTemplateRef} /></Box>

                    <Typography variant='h5'>Пример для теста</Typography>
                    <Box sx={{ width: '100%', height: '100%', background: '#ccc', padding: '4px' }}><div style={{ height: '100%' }} ref={nodeExampleRef} /></Box>
                </Haf>
            </Main>
            <Button variant='contained' onClick={() => run()}>Запустить тесты</Button>
            <Box p={1}>
                <List>
                    {results?.map((result, index) => (
                        <ListItem key={index} sx={{ bgcolor: result?.pass ? 'success.light' : 'error.light' }}>
                             <ListItemAvatar>
                                {result?.pass ? (
                                    <DoneIcon sx={{ color: 'success.main'}} />
                                ) : (
                                    <CloseIcon sx={{ color: 'error.main' }} />
                                )}
                            </ListItemAvatar>
                            <Typography
                                variant='subtitle1' 
                                sx={{ color: 'white', fontWeight: 'bold' }}
                            >
                                {result?.pass ? 'Пройден' : 'Провален'}
                                {' '}
                                Ожидание: {String(result?.expect)}
                                {' '}
                                Получено: {String(result?.output)}
                            </Typography>
                        </ListItem>
                    ))}
                </List>
            </Box>
        </>
    )
}


export default withTask(TestsTab)