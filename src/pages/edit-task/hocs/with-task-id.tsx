import React, { useEffect } from 'react';
import Box from '@mui/material/Box'
import Loader from '@mui/material/CircularProgress'
import { Navigate, useParams } from 'react-router-dom';

import { URLs } from '../../../__data__/urls';
import { useLazyCreateTaskQuery, useLazyGetTaskDataQuery } from '../../../__data__/api';

export const withTask = (Component) => () => {
    const { taskId } = useParams();
    const [trigger, answer] = useLazyCreateTaskQuery();
    const [getTaskData, { data }] = useLazyGetTaskDataQuery();

    useEffect(() => {
        if(!taskId && answer.isUninitialized) {
            trigger(void 0);
        }

        if (taskId && !data) {
            getTaskData(taskId);
        }
    }, [taskId])

    if (!taskId) {
        if (answer.isSuccess) {
            return (
                <Navigate replace to={URLs.editTask.getUrl(answer.data.taskId)} />
            )
        } else {
            return <Box sx={{ mx: "auto", width: 200, height: '100vh' }}><Loader /></Box>
        }
    }

    if (data) {
        return <Component task={data.task} />
    }

    return null
}
