import i18next from 'i18next';
import React, { useState, useRef, useEffect } from 'react';
import Box from '@mui/material/Box';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import EditIcon from '@mui/icons-material/Edit';
import Typography from '@mui/material/Typography';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import { Description } from '@mui/icons-material';

import { useWebSocket } from '../../hooks/websocket';
import { Task } from '../../__data__/model';
import { actions, selectors } from '../../__data__/slices/edit-task';
import { Markdown } from '../../components';

import { withTask } from './hocs/with-task-id';
import { Wrapper, Haf, Main } from './styles';
import { useMonako } from '../../hooks/useMonaco';

const DescriptionTab: React.FC<{ task: Task }> = ({ task: currentTaskParams }) => {
    const dispatch = useDispatch();
    const [editTitle, setEditTitle] = useState(false);
    const [editTags, setEditTags] = useState(false);
    const [toggle, triggerToggle] = useState(false);
    const descr = useSelector(selectors.description);
    const [tags, setTags] = useState<string>();
    const [title, setTitle] = useState<string>();
    const { taskId } = useParams();
    const { saveTask } = useWebSocket();
    const task = useRef<Task>({ ...currentTaskParams });
    useEffect(() => {
        dispatch(actions.changeDescr(currentTaskParams.description));
    }, [currentTaskParams?.description])
    const { nodeRef } = useMonako(currentTaskParams.description, toggle, 'markdown', (description) => {
        dispatch(actions.changeDescr(description));
        saveTask({
            id: taskId,
            ...task.current,
            description
        });
    })

    useEffect(() => {
        setTags(currentTaskParams.tags?.join(', '));
        setTitle(currentTaskParams.title);
    }, [JSON.stringify(currentTaskParams), descr])

    const createChangeHandler = (fieldName) => ({ target: { value } }) => {
        task.current[fieldName] = value;

        setTitle(value);
        saveTask({ id: taskId, ...task.current } as Task);
    }

    const handleTagsChange = ({ target: { value }}) => {
        task.current.tags = value.split(',').map(t => t.replace(/^\s/g, ''));
        setTags(value);

        saveTask({ id: taskId, ...task.current } as Task);
    }

    const handleOutsideFieldClick = () => {
        setEditTitle(false);
        setEditTags(false);
    }

    return (
        <Main size="large">
            <Haf>
                {editTitle ? (
                    <ClickAwayListener onClickAway={handleOutsideFieldClick}>
                        <TextField label={i18next.t('tasks.edit.task.title')} value={title} onChange={createChangeHandler('title')} />
                    </ClickAwayListener>
                ) : (
                    <Typography style={{ cursor: 'pointer' }} onClick={() => setEditTitle(true)} variant="h4">{task.current.title}<EditIcon /></Typography>
                )}
                {editTags ? (
                    <ClickAwayListener onClickAway={handleOutsideFieldClick}>
                        <TextField label="Тэги (не переведено)" value={tags} onChange={handleTagsChange} />
                    </ClickAwayListener>
                ) : (
                    <Typography
                        style={{ cursor: 'pointer' }}
                        onClick={() => setEditTags(true)}
                    >
                        {task.current.tags?.join(', ') || 'Введите сюда тэги через запятую (не переведено)'}
                        <EditIcon />
                    </Typography>
                )}

                <Box sx={{ width: '100%', height: '100%', background: '#ccc', padding: '4px' }}><div style={{ height: '100%' }} ref={nodeRef} /></Box>
            </Haf>
            <Haf>
                <Box p={1} style={{ overflowY: 'scroll' }}>
                    <Markdown markdown={descr} />
                </Box>
            </Haf>
        </Main>
    )
}

export default withTask(DescriptionTab)