const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const pkg = require('./package');

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        },
        module: {
            rules: [
                // {
                //     test: /\.css$/,
                //     use: ['style-loader', 'css-loader', 'postcss-loader']
                // },
                // {
                //     test: /\.ttf$/,
                //     use: ['file-loader']
                // }
            ]
        },
        plugins: [new MonacoWebpackPlugin()]
    },
    navigations: {
        'tasks.main': '/tasks',
        'link.tasks.list': '/list',
        'link.tasks.new': '/new-task',
        'link.tasks.edit': '/edit-task/:taskId',
        'link.tasks.single': '/t/:taskId',
        'link.tasks.single.answer': '/t/:taskId/:answerId',
    },
    features: {
        'tasks': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        'tasks.api.key': '/api',
        'tasks.api.list': '/tasks',
        'tasks.api.create.answer': '/answer/new',
        'tasks.api.create.task': '/task/new',
        'tasks.api.get.task': '/task/:taskId',
        'tasks.ws': '/',
        'tasks.ws.path': '/sosket.io/'
    }
}